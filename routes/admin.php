<?php
// For Language
Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        Session::put('locale', $locale);
    }
    return redirect()->back();
});

Route::group(['prefix' => 'admin','namespace'=>'admin'], function () {

    // Without Login
    Route::get('login', 'AuthController@login');
    Route::post('login/store', 'AuthController@store');

    // Must Login
    Route::group(['middleware' => 'admin:webAdmin'], function(){

        ######################### Home #################################
        Route::get('/', 'HomeController@index');
        Route::get('/index', 'HomeController@index');

        ######################### Logout #################################
        Route::get('/logout', 'AuthController@logout');

        ######################### Admins #################################
        Route::get('admins', 'AdminController@index');
        Route::get('admins/create', 'AdminController@create');
        Route::post('admins/store', 'AdminController@store');
        Route::get('admins/edit/{id?}', 'AdminController@edit');
        Route::patch('admins/update/{id?}', 'AdminController@update');
        Route::post('admins/destroy', 'AdminController@destroy');

        ######################### Settings #################################
        Route::resource('/settings', 'SettingController');

        ######################### Videos #################################
        Route::resource('/videos', 'VideoController');
        Route::post('videos/delete', 'VideoController@destroy');

        ######################### Members #################################
        Route::resource('/members', 'MemberController');
        Route::post('members/delete', 'MemberController@destroy');
        Route::get('members/active/{id}', 'MemberController@active');


    });
});
