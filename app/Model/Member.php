<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
  protected $fillable =
  [
      "name", "id_number", "mobile", "gender", "nationality",
      "birth_date", "address", "image", "status", "sport_id"
  ];


  /**
* @return \Illuminate\Database\Eloquent\Relations\belongsTo
*/
  public function sport()
  {
      return $this->belongsTo(Sport::class,'sport_id')->withDefault(['title' => '']);
  }

}
