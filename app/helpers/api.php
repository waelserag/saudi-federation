<?php
/**
* [sendResponse description]
* @param  [string] $message [message success]
* @param  [array] $data    [any data object or array]
* @return [json]          [json success]
*/
function sendResponse($message,$data){
    return response([
        "status" => true,
        "message" => $message,
        "data"    => $data,
    ],200);
}
/**
* [sendError description]
* @param  [string] $message [message error]
* @return [array]          [errors]
*/
function sendError($message){
    return response([
        "status" => false,
        "message" => $message,
    ],400);
}
