<?php
use App\Model\Contact;
use App\User;
use Illuminate\Support\Facades\Cache;

/**
* [aurl To Go To Admin Url]
* @param  [string] $url [link]
* @return [string]      [/admin.$url]
*/
function aurl($url)
{
    return url('/admin' . $url);
}


/**
* [getUserIP]
* @return [ip] [IP Address]
*/
function getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}

/**
* [GetSetting]
* @param [type] $key [Get Value Of This Key]
*/
function GetSetting($key) {
    try {
        $cached = Cache::rememberForever($key,function() use ($key) {
            return App\Setting::where('key',$key)->first();
        });
        return $cached->type != 'image' ? $cached->value : asset('uploads/'.$cached->value);
    } catch (\Exception $e) {
        throw new \Exception("Error In Settings $key Dose not exist", 1);

    }
}


/**
 * [GetMessage of Visitors]
 */
function GetMessageVisitors() {
    try {
        $s = Contact::where('views','like','0')->orderBy('id', 'desc')->get();
        return $s;
    } catch (\Exception $e) {
        throw new \Exception("Error In Message Dose not exist", 1);

    }
}

/**
 * [CountMessage of Visitors]
 */
function CountMessageVisitors() {
    try {
        $s = Contact::where('views','like','0')->count();
        return $s;
    } catch (\Exception $e) {
        throw new \Exception("Error In Message Dose not exist", 1);

    }
}
