<?php

/**
* [UploadImages]
* @param [folder] $dir   [FolderName]
* @param [string] $image [Name Of The Image]
*/
function UploadImages($dir, $image)
{
    $saveImage = '';
    if (! File::exists(public_path('uploads').'/' . $dir)) { // if file or fiolder not exists
        /**
        *
        * @param $PATH Required
        * @param $mode Defualt 0775
        * @param create the directories recursively if doesn't exists
        */
        File::makeDirectory(public_path('uploads') . '/' . $dir, 0775, true); // create the dir or the
    }
    if (File::isFile($image)) {
        $name       = $image->getClientOriginalName(); // get image name
        $extension  = $image->getClientOriginalExtension(); // get image extension
        $sha1       = sha1($name); // hash the image name
        $fileName   = rand(1, 1000000) . "_" . date("y-m-d-h-i-s") . "_" . $sha1 . "." . $extension; // create new name for the image
        // get the image realpath
        $uploadedImage = Image::make($image->getRealPath());
        $uploadedImage->save(public_path('uploads/' . $dir . '/' . $fileName), '100'); // save the image
        $saveImage = $dir . '/' . $fileName; // get the name of the image and the dir that uploaded in
    }
    return $saveImage;
}

/**
* [uploadMultiImages ]
* @param  [type] $images   [Request Image]
* @param  [type] $old      [Old Image]
* @param  [type] $elImages [images in database]
* @param  [type] $destination [Destination]
* @return [type]           [Upload Multi Image]
*/
function uploadMultiImages($images, $old, $elImages, $destination)
{
    unlinkRemovedImages($old, $elImages);
    $imgs = [];
    if (!is_null($images)) {
        foreach ($images as $i) {
            $imgs[] = UploadImages($destination, $i);
        }
    }
    if(empty($old)){
        $old = [];
    }
    return implode('|', array_merge($old, $imgs));
}


/**
* [UpdateImages]
* @param [string] $oldFile [old image]
* @param [folder] $dir     [folder of image]
* @param [string] $image   [new image]
*/
function UpdateImages($oldFile, $dir, $image) {
    if ( !empty($oldFile) && !is_null($oldFile) && file_exists( public_path('uploads/'.$oldFile) ) ) {
        unlink(public_path('uploads/'.$oldFile));
    }
    return UploadImages($dir, $image);
}
/**
* [checkValue]
* @param  [string] $value [Check if it Found Or No]
* @return [Bollean]        [True Or False]
*/
function checkValue($value)
{
    return !empty($value) && !is_null($value);
}

/**
* [unlinkRemovedImages]
* @param  [type] $old      [description]
* @param  [type] $elImages [description]
* @return [type]           [description]
*/
function unlinkRemovedImages($old, $elImages)
{
    $elImages = explode('|', $elImages);

    $removedImages = array_diff($elImages, $old ?? []);

    if (count($removedImages) && !is_null($removedImages)) {
        foreach ($removedImages as $i) {
            if (!is_null($i) && !empty($i)) {
                @unlink('uploads/'.$i);
            }
        }
    }
}
