<?php

namespace App\Http\Requests\video;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;
use App\Rules\ValidateYoutube;

class VideoUpdateRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|max:200',
            'link'  =>'required|url',
            'link'  =>[new ValidateYoutube],
        ];
    }

    /**
    *  Filters to be applied to the input.
    *
    * @return array
    */
   public function filters()
   {
       return [
           'title' => 'trim|escape',
           'link'  => 'trim',
       ];
   }

}
