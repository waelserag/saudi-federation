<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class AdminStoreRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=>'required|max:200',
            'email'   =>'required|min:6|max:200|email|unique:admins,email',
            'phone'   =>'required|numeric|unique:admins,phone',
            'password'=>'required|min:6|max:30',
        ];
    }

    /**
    *  Filters to be applied to the input.
    *
    * @return array
    */
   public function filters()
   {
       return [
           'username' => 'trim|escape|capitalize',
           'email'    => 'trim|escape',
           'phone'    => 'trim|escape|digit',
           'password' => 'trim',
       ];
   }

}
