<?php

namespace App\Http\Requests\member;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;
use App\Rules\ValidateYoutube;

class MemberUpdateRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        =>'required|string|max:200',
            'id_number'   =>'required|min:10|max:17|unique:members,id_number,'.$this->member->id,
            'mobile'      =>'required|min:10|max:17',
            'gender'      =>'required|in:1,2',
            'nationality' =>'required|string|max:200',
            'birth_date'  =>'required|date|before:2018-01-01|after:1930-01-01',
            'address'     =>'required|string|max:200',
            'image'       =>'sometimes|nullable|image',
            'sport_id'    =>'required|exists:sports,id',
        ];
    }

    /**
    *  Filters to be applied to the input.
    *
    * @return array
    */
   public function filters()
   {
       return [
           'name'       => 'trim|escape',
           'id_number'  => 'trim|digit|escape',
           'mobile'     => 'trim|digit|escape',
           'gender'     => 'trim|digit|escape',
           'nationality'=> 'trim|escape',
           'birth_date' => 'trim',
           'address'    => 'trim|escape',
           'sport_id'   => 'trim|digit',
       ];
   }

}
