<?php
namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\member\MemberStoreRequest;
use App\Model\Member;
use App\Model\Visitor;
use App\Model\Sport;

class MemberController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Add New Visit If This First Visit In Today
        $user_ip = getUserIP();
        $created = date("Y-m-d");
        $visitor = Visitor::where('ip', $user_ip)->where('created',$created)->first();
        if(is_null($visitor)){
            Visitor::create(['ip' => $user_ip,'created' => $created]);
        }

        $sports = Sport::get();
        return view('front.members.create',compact('sports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberStoreRequest $request)
    {
        $data = $request->validated();
        //If Membership is Planes make membership wait active
        $request->sport_id == "2" ? $data['status'] = '2' : $data['status'] = '1 '; //2 Not Active && 1 Active
        //Upload Image
        $destination = "members/" . date("Y") . "/" . date("m");
        $data['image'] = UploadImages($destination, $request->file('image'));
        //Create New Member
        Member::Create($data);
        //Return Success Message
        return redirect()->back()->with(['success' => trans('admin.subscription_message')]);
    }

}
