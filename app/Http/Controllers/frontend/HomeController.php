<?php
namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use App\Model\Visitor;

class HomeController extends Controller
{

    /**
     * [index Home]
     * @return [type] [description]
     */
    public function index()
    {
        //Add New Visit If This First Visit In Today
        $user_ip = getUserIP();
        $created = date("Y-m-d");
        $visitor = Visitor::where('ip', $user_ip)->where('created',$created)->first();
        if(is_null($visitor)){
            Visitor::create(['ip' => $user_ip,'created' => $created]);
        }
        return view('front.index');
    }

}
