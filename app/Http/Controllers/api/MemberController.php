<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Member;

class MemberController extends Controller
{
    /**
     * [Check Member]
     * @param  Request $request [id_number]
     * api_url: api/status  [method:post]
     * @return [json]           [Member data with Message]
     */
    public function index()
    {
        // search if member found and active
        $member = Member::where('status','1')->get(['id_number'])->pluck('id_number');
        // all good so return success
        return response()->json($member);
    }


}
