<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Member;
use App\Model\Visitor;

class HomeController extends Controller
{

    /**
     * [index Admin]
     * @return [type] [description]
     */
    public function index()
    {
        //Visits
        $created = date("Y-m-d");
        $today = Visitor::where('created',$created)->count();
        $createdS = strtotime($created);
        $date7 = strtotime("-7 day", $createdS);
        $date7= date('Y-m-d', $date7);
        $date7 = Visitor::whereBetween('created', [$date7, $created])->count();
        $date30 = strtotime("-30 day", $createdS);
        $date30= date('Y-m-d', $date30);
        $date30 = Visitor::whereBetween('created', [$date30, $created])->count();
        $date365 = strtotime("-365 day", $createdS);
        $date365= date('Y-m-d', $date365);
        $date365 = Visitor::whereBetween('created', [$date365, $created])->count();
        $total = Visitor::count();

        //Members
        $unactive_members = Member::where('status','0')->count();
        $active_members   = Member::where('status','1')->count();
        $all_members      = $unactive_members+$active_members;
        return view('admin.index',compact('today','date7','date30',
        'date365','total', 'unactive_members', 'active_members', 'all_members'));
    }

}
