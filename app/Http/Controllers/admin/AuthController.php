<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * [login Admin]
     * @return [type] [description]
     */
    public function login(){
        if(auth()->guard('webAdmin')->check()){
            return redirect('/admin/index');
        }
        return view('admin.login');
    }

    /**
     * [store description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        //Validation
        $data = $this->validate($request, [
        'email'=>'required|email',
        'password'=>'required|min:6',
        ]);
        //IF Check On Remember Me
        if($request->remember == "on"){
            $remember = true;
        }else{
            $remember = false;
        }
        //Succes Message
        if(auth()->guard('webAdmin')->attempt($data,$remember)){
            return redirect('admin/index')->with([
            'message' => trans('auth.login_success'),
            ]);
        }
        //Error  Message
        return redirect('admin/login')->with([
        'error' => trans('auth.failed'),
        ]);
    }

    /**
     * [logout description]
     * @return [type] [description]
     */
    public function logout()
    {
        auth()->guard('webAdmin')->logout();
        return redirect('/admin/login');
    }


}
