<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Model\Setting;


class SettingController extends Controller
{
    private $rules = [
        'key'     => 'required|max:250',
        'type'     => 'required|in:text,longtext,image',
    ];

    private $rulesUpdate = [
        'value'     => 'required',
    ];

    private $rulesUpdateImage = [
        'value'     => 'required|image',
    ];

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $setting = Setting::all();
        return view('admin.settings.create', [
            'title' => "Create setting",
            'setting' => $setting,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, $this->rules);
        $setting = Setting::create($data);
        session()->flash('success', "add Successfully");
        return redirect()->route('settings.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function index()
     {
         $pages = Setting::all();
         return view('admin.settings.index', [
             'title' => trans('admin.settings'),
             'index' => $pages,
         ]);
     }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::findOrFail($id);
        return view('admin.settings.edit', [
                'title' => trans('admin.edit settings'),
                'edit'  => $setting,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $setting = Setting::findOrFail($id);
        Cache::forget($setting->key);

        if ($setting->type == "image") {
            $data = $this->validate($request, $this->rulesUpdateImage);
        if ($request->hasFile('value')) {
            if (file_exists(public_path('uploads/settings' . $setting->value))) {
                @unlink(public_path('uploads/settings' . $setting->value));
            }
            $data['value'] = UploadImages('settings', $request->file('value'));
        }
        $data = array_merge($request->except('_token', '_method'), $data);
      }else{
        $data = $this->validate($request, $this->rulesUpdate);
        $data['value'] = $request->value;
      }

        $setting->update($data);

        session()->flash('success', trans('admin.edit Successfully'));
        return redirect()->route('settings.index');
    }



}
