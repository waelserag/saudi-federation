<?php
namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Video;
use App\Http\Requests\video\VideoStoreRequest;
use App\Http\Requests\video\VideoUpdateRequest;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.videos.index',[
            'title'=>trans('admin.Videos'),
            'videos'=>Video::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.videos.create',[
            'title'=>trans('admin.add video'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(VideoStoreRequest $request)
     {

         $data = $request->validated();
         $admin = Video::Create($data);

         return redirect(aurl('/videos'))->with(['success' => trans('admin.add Successfully')]);
     }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::findOrFail($id);
        return view('admin.videos.edit', [
            'title' => trans("admin.edit video") . ' : ' . $video->title,
            'video'  => $video,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(VideoUpdateRequest $request,$id)
     {

         $video = Video::findOrFail($id);
         //Validation
         $data = $request->validated();
         //Update Video
         $video->update($data);
         // Success Message
         session()->flash('success', trans("admin.edit Successfully"));
         return  redirect('admin/videos');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        if (request()->filled('id')) {
            $id = request()->id;
            Video::findOrFail($id)->delete();
        }
    }
}
