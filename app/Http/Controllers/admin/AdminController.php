<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use Hash;
use App\Http\Requests\admin\AdminStoreRequest;
use App\Http\Requests\admin\AdminUpdateRequest;

class AdminController extends Controller
{

    /**
     * List Of Admins
     * @return [Admins]
     */
    public function index(){

        return view('admin.admins.index',[
            'title'=>trans('admin.admins'),
            'admins'=>Admin::orderBy('created_at','desc')->get()
        ]);
    }

    /**
     * [create New Admin]
     * @return [Create Page]
     */
    public function create(){

        return view('admin.admins.create',[
            'title'=>trans('admin.add admin'),
        ]);
    }

    /**
     * [store new admin]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(AdminStoreRequest $request)
    {

        $data = $request->validated();

        $data['password'] = Hash::make($request->password);
        $admin = Admin::Create($data);

        return redirect(aurl('/admins'))->with(['success' => trans('admin.add Successfully')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        if(empty($id)){
            $id = auth()->guard('webAdmin')->id();
        }
        $admin = Admin::findOrFail($id);
        return view('admin.admins.edit', [
            'title' => trans("admin.edit admins") . ' : ' . $admin->username,
            'admin'  => $admin,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id = null)
    {
        if(empty($id)){
            $id = auth()->guard('webAdmin')->id();
        }
        $admin = Admin::findOrFail($id);
        //Validation
        //Validation
        $this->rules['email'] = 'required|unique:admins,email,'.$id;
        $this->rules['username'] = 'required|max:200';
        $this->rules['phone'] = 'required|numeric|unique:admins,phone,'.$id;

        $data = $this->validate($request, $this->rules);
        //Make Hash To Password
        if($request->password){
            $data['password'] = Hash::make($request->password);
        }
        //Update Admin
        $admin->update($data);
        // Success Message
        session()->flash('success', trans("admin.edit Successfully"));
        return  redirect('admin/admins');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  bool  $redirect
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        if (request()->filled('id')) {
            $id = request()->id;
            Admin::findOrFail($id)->delete();
        }
    }

}
