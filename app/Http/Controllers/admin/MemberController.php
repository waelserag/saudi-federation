<?php
namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Member;
use App\Model\Sport;
use App\Http\Requests\member\MemberStoreRequest;
use App\Http\Requests\member\MemberUpdateRequest;
use EloquentBuilder;

class MemberController extends Controller
{
    /**
     * [Filter Function]
     * @param string $value [description]
     */
    public function filter($member)
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get Sports For Select
        $sports = Sport::get();

        //Make Advanced Filter Search
        $member = EloquentBuilder::to(
                    Member::class,
                    request()->all()
                 );
        //End Search
        $count = $member->count(); // For Get Count of Members
        $members = $member->with('sport')->paginate(30);
        return view('admin.members.index',[
            'title'=>trans('admin.Members'),
            'members'=>$members,
            'sports'=>$sports,
            'count'=>$count
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sports = Sport::get();
        return view('admin.members.create',[
            'title'  =>trans('admin.add member'),
            'sports' => $sports
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(MemberStoreRequest $request)
     {

         $data = $request->validated();
         //If Membership is Planes make membership wait active
         $request->sport_id == "2" ? $data['status'] = '2' : $data['status'] = '1 ';
         //Upload Image
         $destination = "members/" . date("Y") . "/" . date("m");
         $data['image'] = UploadImages($destination, $request->file('image'));
         //Create New Member
         Member::Create($data);
         //Return Success Message
         return redirect(aurl('/members'))->with(['success' => trans('admin.add Successfully')]);
     }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::findOrFail($id);
        $sports = Sport::get();
        return view('admin.members.edit', [
            'title' => trans("admin.edit member") . ' : ' . $member->name,
            'member'  => $member,
            'sports'  => $sports,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(MemberUpdateRequest $request,Member $member)
     {

         $member = Member::findOrFail($member->id);
         //Validation
         $data = $request->validated();
         //If Membership is Planes make membership wait active
         $request->sport_id == "2" ? $data['status'] = '2' : $data['status'] = '1 ';
         // Upload Image
         if ($request->hasFile('image')) {
             if (file_exists(public_path('uploads/' . $member->image))) {
                 @unlink(public_path('uploads/' . $member->image));
             }
             $destination = "members/" . date("Y") . "/" . date("m");
             $data['image'] = UploadImages($destination, $request->file('image'));
         }else{
              $data['image'] = $member->image;
         }
         //Update Member
         $member->update($data);
         // Success Message
         session()->flash('success', trans("admin.edit Successfully"));
         return  redirect('admin/members');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        if (request()->filled('id')) {
            $id = request()->id;
            $member = Member::findOrFail($id);
            if (file_exists(public_path('uploads/' . $member->image))) {
                @unlink(public_path('uploads/' . $member->image));
            }
            $member->delete();
        }
    }



    /**
     * Show the form for active or deactive the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        //Update Data
        $member = Member::findOrFail($id);
        if ($member->status == 1) {
            $member->update(['status' => '0']);
        } else {
            $member->update(['status' => '1']);
        }
        // Success Message
        session()->flash('success', trans("admin.edit Successfully"));
        return  redirect('admin/members');
    }

}
