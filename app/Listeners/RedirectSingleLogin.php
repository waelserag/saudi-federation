<?php

namespace App\Listeners;

use App\Events\SingleLogin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redirect;

class RedirectSingleLogin  implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SingleLogin  $event
     * @return void
     */
    public function handle(SingleLogin $event)
    {
        $pervious_link = $event->url;
        $code = auth()->id()*545323259005;
        if (strpos($pervious_link,'?') != false) {
            $links = explode('?',$pervious_link);
            $link = $links[1];
            return  redirect($link.'?985'.$code);
        }
         if (strpos($pervious_link,'?') != true) {
            return  redirect('https://arabedutech.tk/study/public/dashboard?000985'.$code);
        }
    }
}
