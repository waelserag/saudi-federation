<?php

use Illuminate\Database\Seeder;
use App\Model\Admin;


class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Admin();
        $admin->username = 'Super Admin';
        $admin->email = "admin@tahakoom.gov.sa";
        $admin->phone = "01020104730";
        $admin->password =  bcrypt("waelseragwaelserag");
        $admin->save();
    }
}
