<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test example.
     *@test
     * @return void
     */
    // public function confirmFullNameTest()
    // {
    //     $user = User::create([
    //         'first_name' => "Ahmed",
    //         'last_name'  => "Serag",
    //         'email'      => "waelserag1@gmail.com",
    //         'password'   => "secret",
    //     ]);
    //     $this->assertEquals("Wael Serag",$user->fullname);
    // }

    /**
     * A basic unit test example.
     *@test
     * @return void
     */
    public function firstNameRequired()
    {
        $user = factory(User::class)->make();
        $this->assertNotEmpty($user->first_name);
    }

    /**
     * A basic unit test example.
     *@test
     * @return void
     */
    public function passwordRequired()
    {
        $user = factory(User::class)->make();
        $this->assertNotEmpty($user->password);
    }
}
