// // init eva icons 
// eva.replace()

// jQuery code

$(function () {

  // WOW animation on scroll 
  new WOW().init({
    offset: 0,          // default
  });

  // menu button toggler 
  $('.header .navbar-toggler').on('click', function () {
    $('.header .navbar-toggler').toggleClass('active-m');
  });

  // home slider 
  $('.home-carousel').carousel({
    interval: 400000
  })

  $('.m-card').mouseover(function () {
    // console.clear();
  });

  // Pixel Btns 
  $('.btn').prepend('<div class="hover"><span></span><span></span><span></span><span></span><span></span></div>');
  $('.social-btn').prepend('<div class="hover"><span></span><span></span><span></span><span></span></div>');


  // competions
  $(".competition .col-12").on("click", function () {
    $(".competition .col-12, .m-card .comp-icon, .m-card .comp-h, .m-card p, .m-card .btn-circle i").toggleClass('d-none');
    $(this).toggleClass("col-md-6 col-lg-3 d-none");
  })
  $(".competition .btn-circle").on("click", function (e) {
    e.preventDefault();
  })

});





$(document).ready(function () {
  $("#example").DataTable({
    aaSorting: [],
    responsive: true,

    columnDefs: [
      {
        responsivePriority: 1,
        targets: 0
      },
      {
        responsivePriority: 2,
        targets: -1
      }
    ]
  });

  $(".dataTables_filter input")
    .attr("placeholder", "Search here...")
    .css({
      width: "300px",
      display: "inline-block"
    });

  $('[data-toggle="tooltip"]').tooltip();
});

// add extra form line 
$(function () {

  // CO_X CO_REQ CO_STAR
  var coachHtml = ` <div class="row coach-row rel-pos bt-1 pt-3" data-coach-num="CO_X"> 
  
  <span class="delete-row delete-row-coach">X</span>
  <div class="form-label-group col-12 col-md-4 " >
    <input
      type="text"
      id="CoachNameCO_X"
      class="form-control"
      placeholder="Coach Name CO_X"
      required="CO_REQ"
      autofocus=""
    />
    <label for="CoachNameCO_X"> Coach Name CO_X <i>CO_STAR</i></label>
  </div>

  <div class="form-label-group col-12 col-md-4 " >
    <input
      type="email"
      id="CoachEmailCO_X"
      class="form-control"
      placeholder="Coach Email"
      required="CO_REQ"
      autofocus=""
    />
    <label for="CoachEmailCO_X">Coach Email <i>CO_STAR</i></label>
  </div>

  <div class="form-label-group col-12 col-md-4 " >
    <input
      type="tel"
      id="CoachMobileCO_X"
      class="form-control"
      placeholder="Coach Mobile"
      required="CO_REQ"
      autofocus=""
    />
    <label for="CoachMobileCO_X">Coach Mobile <i>CO_STAR</i></label>
  </div>
  <!-- end of row  -->
</div>`


  // CO_X CO_REQ CO_STAR
  var membHtml = ` 
<!-- start form row -->
                        <div class="row Member-row rel-pos bt-1 pt-3" data-Member-num="1">
                        <span class="delete-row delete-row-memb">X</span>
                          <div class="form-label-group col-12 col-md-6 col-lg-3 ">
                            <input
                              type="text"
                              id="MemberNameCO_X"
                              class="form-control"
                              placeholder="Member Name"
                              required="CO_REQ"
                              autofocus=""
                            />
                            <label for="MemberNameCO_X"> Member Name <i>CO_STAR</i></label>
                          </div>

                          <div class="form-label-group col-12 col-md-6 col-lg-3 ">
                            <input
                              type="number"
                              id="NationalIDCO_X"
                              class="form-control"
                              placeholder="National ID"
                              required="CO_REQ"
                              autofocus=""
                            />
                            <label for="NationalIDCO_X"> National ID <i>CO_STAR</i></label>
                          </div>

                          <div class="form-label-group col-12 col-md-6 col-lg-3 ">
                            <input
                              type="tel"
                              id="MemMobileCO_X"
                              class="form-control"
                              placeholder="Mobile "
                              required="CO_REQ"
                              autofocus=""
                            />
                            <label for="MemMobileCO_X"> Mobile  <i>CO_STAR</i></label>
                          </div>

                          <div class="form-label-group col-12 col-md-6 col-lg-3 ">
                            <input
                              type="date"
                              id="MemBirthdateCO_X"
                              class="form-control file-upload px-3"
                              placeholder="Birthdate  "
                              required="CO_REQ"
                              autofocus=""
                            />
                            <label for="MemBirthdateCO_X" class="file-upload-label"> Birthdate   <i>CO_STAR</i></label>
                          </div>

                          <!-- end of row  -->
                        </div>
                        <!-- end form row -->
`


  var counter = 1;
  var MemCounter = 2;


  var disapleBtn = (x) => $(`.add-new-${x}`).attr("disabled", "true");


  $(".add-new-coach").on("click", function (ee) {
    // check number of coaches 
    ee.preventDefault();

    if (coaches.length > counter) {

      var newCoachHtml = coachHtml.replace(/CO_X/g, coaches[counter].x);
      newCoachHtml = newCoachHtml.replace(/CO_REQ/g, coaches[counter].required);

      if (coaches[counter].required) {
        newCoachHtml = newCoachHtml.replace(/CO_STAR/g, "*");
      } else {
        newCoachHtml = newCoachHtml.replace(/CO_STAR/g, " ");
      }

      $(".coach-row").last().after(newCoachHtml);
      counter++;
      if (coaches.length <= counter) disapleBtn('coach');
    } else {
      disapleBtn('coach')
    }

  });
  /////// 
  // members
  $(".add-new-member").on("click", function (e) {
    // check number of members 
    e.preventDefault();
    if (members.length > MemCounter) {

      var newCoachHtml = membHtml.replace(/CO_X/g, members[MemCounter].x);
      newCoachHtml = newCoachHtml.replace(/CO_REQ/g, members[MemCounter].required);

      if (members[MemCounter].required) {
        newCoachHtml = newCoachHtml.replace(/CO_STAR/g, "*");
      } else {
        newCoachHtml = newCoachHtml.replace(/CO_STAR/g, " ");
      }

      $(".Member-row").last().after(newCoachHtml);
      MemCounter++;
      if (members.length <= MemCounter) disapleBtn('member');
    } else {
      disapleBtn('member')
    }

  });


  var counterMin = (x) => {
    if (x == 'coach') counter--;
    if (x == 'member') MemCounter--;
    $(`.add-new-${x}`).removeAttr("disabled")
  }




  // //delete coach
  $(document).on("click", ".delete-row-coach", function (e) {
    e.preventDefault();
    $(this).parent().remove();
    counterMin('coach');
  });
  // delete member
  $(document).on("click", ".delete-row-memb", function (e) {
    e.preventDefault();
    $(this).parent().remove();
    counterMin('member');
  });


});