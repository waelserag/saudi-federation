<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //main
    'dashboard'     => 'الرئيسية',
    'details'       => 'التفاصيل',
    'menu'          => 'القائمة',
    'messages'      => 'الرسائل',
    'settings'      => 'الإعدادات',
    'edit settings' => 'تعديل الإعدادات',
    'hello'         => 'مرحبا',
    'Cpanel'        => 'لوحة التحكم',
    'new'           => 'جديد',
    'image'         => 'صورة',
    'male'          => 'ذكر',
    'female'        => 'أنثى',
    'search'        => 'بحث',
    'status'        => 'الحالة',
    'active'        => 'تفعيل',
    'unactive'      => 'إلغاء التفعيل',

    //Visitors
    'visitors'      => 'الزيارات',
    'today_visitors'=> 'عدد زيارات اليوم',
    'all_visitors'  => 'إجمالي الزيارات',

    //Buttons
    'Copy' => 'نسخ',
    'Excel' => 'اكسل',
    'PDF' => 'PDF',
    'Column visibility' => 'ظهور الأعمدة',
    'name' => 'الإسم',
    'date' => 'التاريخ',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'show' => 'عرض',
    'save' => 'حفظ',
    'actions' => 'عمليات',
    'key' => 'العنوان',
    'value' => 'المحتوى',

    //Messages
    'add Successfully' => 'لقد تم الحفظ بنجاح',
    'edit Successfully' => 'لقد تم التعديل بنجاح',
    'delete Successfully' => 'لقد تم الحذف بنجاح',
    'sent Successfully' => 'لقد تم إرسال الرسالة بنجاح',
    'subscription_message' => 'لقد تم الإشتراك بنجاح واصبحت عضو في الاتحاد السعودي للرياضات اللاسلكية والتحكم عن بعد ويمكنك الآن المشاركة في المسابقات',

    //Sponsors
    'add logo' => 'إضافة راعي',
    'add logos' => 'إضافة رعاة',
    'Add Logo' => 'إضافة راعي',
    'edit logo' => 'تعديل راعي',
    'show logo' => 'عرض الراعي',
    'show all logos' => 'عرض جميع الرعاة',
    'all logo' => 'جميع الرعاة',
    'logo' => 'الراعي',
    'Logos' => 'الرعاة',

    //Admins
    'add admin' => 'إضافة مدير',
    'add admins' => 'إضافة مديرين',
    'Add Admin' => 'إضافة مدير',
    'edit admins' => 'تعديل مدير',
    'show admin' => 'عرض مدير',
    'show all admins' => 'عرض جميع المديرين',
    'all admins' => 'جميع المديرين',
    'admin' => 'المدير',
    'admins' => 'المديرين',

    //Videos
    'add video' => 'إضافة فيديو',
    'add videos' => 'إضافة فيديوهات',
    'Add Video' => 'إضافة فيديو',
    'edit video' => 'تعديل الفيديو',
    'show video' => 'عرض الفيديو',
    'show all videos' => 'عرض جميع الفيديوهات',
    'all video' => 'جميع الفيديوهات',
    'video' => 'الفيديو',
    'Videos' => 'الفيديوهات',
    'validate_youtube' => 'يجب كتابة رابط اليوتيوب بشكل صحيح من موقع اليوتيوب',

    //Members
    'add member' => 'إضافة عضوية',
    'add members' => 'إضافة عضوية',
    'Add Member' => 'إضافة عضوية',
    'edit member' => 'تعديل عضوية',
    'show member' => 'عرض عضوية',
    'show all members' => 'عرض جميع الأعضاء',
    'all member' => 'جميع الأعضاء',
    'member' => 'العضو',
    'Members' => 'الأعضاء',

    //Setting Page
    'email' => 'البريد الإلكتروني',
    'facebook' => 'رابط الفيس بوك',
    'logo_home' => 'الشعار',
    'mobile' => 'الجوال',
    'snapchat' => 'رابط سناب شات',
    'twitter' => 'رابط تويتر',
    'youtube' => 'رابط اليوتيوب',
    'whatsapp' => 'الواتساب',





];
