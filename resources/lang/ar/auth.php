<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login_success' => 'لقد تم تسجيل الدخول بنجاح',
    'failed' => 'برجاء التأكد من البيانات',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'I accept' => 'أوافق على',
    'Terms & Conditions' => 'الشروط والخصوصية',
];
