<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are useD during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //Main
    'dashboard'     => 'Dashboard',
    'details'       => 'Details',
    'menu'          => 'Main',
    'messages'      => 'Messages',
    'settings'      => 'Settings',
    'edit settings' => 'Edit Settings',
    'hello'         => 'Hello',
    'Cpanel'        => 'Cpanel',
    'new'           => 'New',
    'image'         => 'Image',
    'male'          => 'Male',
    'female'        => 'Female',
    'search'        => 'Search',
    'status'        => 'Status',
    'active'        => 'Active',
    'unactive'      => 'UnActive',


    //Visitors
    'visitors'      => 'Visitors',
    'today_visitors'=> 'Visitors Today',
    'all_visitors'  => 'All visitors',

    //Theme Buttons
    'Copy' => 'Copy',
    'Excel' => 'Excel',
    'PDF' => 'PDF',
    'Column visibility' => 'Column visibility',
    'name' => 'Name',
    'date' => 'Date',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'show' => 'Show',
    'save' => 'Save',
    'actions' => 'Actions',
    'key' => 'Key',
    'value' => 'Value',

    //Messages
    'add Successfully'     => 'Your Data added Successfully',
    'edit Successfully'    => 'Your Data Updated Successfully',
    'delete Successfully'  => 'Your Data Deleted Successfully',
    'sent Successfully'    => 'Your message has been sent successfully',
    'subscription_message' => 'You have successfully participated in the Saudi Federation for Wireless Sports and Remote Control and you can participate in competitions',

    //Sponsors
    'add logo' => 'Add Sponsor',
    'add logos' => 'Add Sponsors',
    'Add Logo' => 'Add Sponsor',
    'edit logo' => 'Edit Sponsor',
    'show logo' => 'Show Sponsor',
    'show all logos' => 'Show All Sponsors',
    'all logo' => 'All Sponsors',
    'logo' => 'Sponsor',
    'Logos' => 'Sponsors',

    //Admins
    'add admin' => 'Add Admin',
    'add admins' => 'Add Admins',
    'Add Admin' => 'Add Admin',
    'edit admins' => 'Edit Admin',
    'show admin' => 'Show Admin',
    'show all admins' => 'Show All Admins',
    'all admins' => 'All Admins',
    'admin' => 'Admin',
    'admins' => 'Admins',

    //Videos
    'add video' => 'Add Video',
    'add videos' => 'Add Videos',
    'Add Video' => 'Add Video',
    'edit video' => 'Edit Video',
    'show video' => 'Show Video',
    'show all videos' => 'Show All Videos',
    'all video' => 'All Videos',
    'video' => 'Video',
    'Videos' => 'Videos',
    'validate_youtube' => 'You must write youtube link correct from youtube site',

    //Members
    'add member' => 'Add Member',
    'add members' => 'Add Members',
    'Add Member' => 'Add Member',
    'edit member' => 'Edit Member',
    'show member' => 'Show Member',
    'show all members' => 'Show All Members',
    'all member' => 'All Members',
    'member' => 'Member',
    'Members' => 'Members',


    //Setting Page
    'email' => 'Email',
    'facebook' => 'Facebook link',
    'logo_home' => 'Logo',
    'mobile' => 'Mobile',
    'snapchat' => 'Snapchat link',
    'twitter' => 'Twitter link',
    'youtube' => 'Youtube link',
    'whatsapp' => 'Whatsapp',





];
