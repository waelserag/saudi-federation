<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login_success' => 'You have successfully logged in',
    'failed' => 'Please Check Your Data',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'I accept' => 'I accept',
    'Terms & Conditions' => 'Terms & Conditions',

];
