@extends('admin.layouts.app')
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">{{ $title }}</h3>

        </div>
    </div>
    <div class="content-body">
        <!-- Striped row layout section start -->
        <section id="striped-row-form-layouts">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="horz-layout-icons">{{ $title }}</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form form-horizontal striped-rows form-bordered" method="post" enctype="multipart/form-data"  action="{{ route('members.update', [$member->id]) }}">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-body">

                                        <!-- Edit Full Name -->
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                for="timesheetinput2">{{ trans('validation.attributes.name') }}</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <input type="text" required class="form-control"
                                                        placeholder="{{ trans('validation.attributes.name') }}" name="name"
                                                        value="{{ $member->name }}">
                                                    <div class="form-control-position">
                                                        <i class="ft-tag"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Edit ID Number -->
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                for="timesheetinput2">{{ trans('validation.attributes.id_number') }}</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <input type="number" required class="form-control"
                                                        placeholder="{{ trans('validation.attributes.id_number') }}" name="id_number"
                                                        value="{{ $member->id_number }}">
                                                    <div class="form-control-position">
                                                        <i class="ft-tag"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Edit Mobile -->
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                for="timesheetinput2">{{ trans('validation.attributes.mobile') }}</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <input type="number" required class="form-control"
                                                        placeholder="{{ trans('validation.attributes.mobile') }}" name="mobile"
                                                        value="{{ $member->mobile }}">
                                                    <div class="form-control-position">
                                                        <i class="ft-tag"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Edit Membership Type -->
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                for="timesheetinput2">{{ trans('validation.attributes.sport_id') }}</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <select
                                                      id="membershipType"
                                                      class="form-control"
                                                      required="true"
                                                      name="sport_id"
                                                    >
                                                      <option value="" selected>{{ trans('validation.attributes.sport_id') }}</option>
                                                      @foreach ($sports as $sport)
                                                          <option value="{{ $sport->id }}" {{ $member->sport_id == $sport->id ? 'selected' : '' }}>{{ $sport->title }}</option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Edit Gender -->
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                for="timesheetinput2">{{ trans('validation.attributes.gender') }}</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <select
                                                      id="genderType"
                                                      class="form-control"
                                                      required="true"
                                                      name="gender"
                                                    >
                                                      <option value="" selected> {{ trans('validation.attributes.gender') }} </option>
                                                      <option value="1" {{ $member->gender == "1" ? 'selected' : '' }}> {{ trans('admin.male') }} </option>
                                                      <option value="2" {{ $member->gender == "2" ? 'selected' : '' }}> {{ trans('admin.female') }} </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Edit Nationality -->
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                for="timesheetinput2">{{ trans('validation.attributes.nationality') }}</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <input type="text" required class="form-control"
                                                        placeholder="{{ trans('validation.attributes.nationality') }}" name="nationality"
                                                        value="{{ $member->nationality }}">
                                                    <div class="form-control-position">
                                                        <i class="ft-tag"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Edit Birth Date -->
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                for="timesheetinput2">{{ trans('validation.attributes.birth_date') }}</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <input type="date" required class="form-control"
                                                        placeholder="{{ trans('validation.attributes.birth_date') }}" name="birth_date"
                                                        value="{{ $member->birth_date }}">
                                                    <div class="form-control-position">
                                                        <i class="ft-tag"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Edit Address -->
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                for="timesheetinput2">{{ trans('validation.attributes.address') }}</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <input type="text" required class="form-control"
                                                        placeholder="{{ trans('validation.attributes.address') }}" name="address"
                                                        value="{{ $member->address }}">
                                                    <div class="form-control-position">
                                                        <i class="ft-tag"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @if (!empty($member->image))
                                            <!-- Show Image -->
                                            <div class="form-group row">
                                                <label class="col-md-3 label-control"
                                                    for="timesheetinput2">{{ trans('validation.attributes.image') }}</label>
                                                <div class="col-md-9">
                                                    <div class="position-relative has-icon-left">
                                                        <img src="{{ asset('uploads/'.$member->image) }}" style="height:200px; width:200px;" alt="{{ $member->name }}">
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <!-- Edit Image -->
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                for="timesheetinput2">{{ trans('validation.attributes.image') }}</label>
                                            <div class="col-md-9">
                                                <div class="position-relative has-icon-left">
                                                    <input type="file" class="form-control"
                                                        name="image">
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <!-- Edit Button -->
                                    <div class="form-actions right">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="la la-check-square-o"></i> {{ trans("admin.edit") }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
