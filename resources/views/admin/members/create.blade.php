@extends('admin.layouts.app')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">{{ $title }}</h3>

    </div>
</div>
<div class="content-body">
    <!-- Striped row layout section start -->
    <section id="striped-row-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-icons">{{ $title }}</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <form class="form form-horizontal striped-rows form-bordered" method="post"
                                action="{{ route('members.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">

                                    <!-- Add Full Name -->
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                            for="timesheetinput2">{{ trans('validation.attributes.name') }}</label>
                                        <div class="col-md-9">
                                            <div class="position-relative has-icon-left">
                                                <input type="text" required class="form-control"
                                                    placeholder="{{ trans('validation.attributes.name') }}" name="name"
                                                    value="{{ old('name') }}">
                                                <div class="form-control-position">
                                                    <i class="ft-tag"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Add ID Number -->
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                            for="timesheetinput2">{{ trans('validation.attributes.id_number') }}</label>
                                        <div class="col-md-9">
                                            <div class="position-relative has-icon-left">
                                                <input type="number" required class="form-control"
                                                    placeholder="{{ trans('validation.attributes.id_number') }}" name="id_number"
                                                    value="{{ old('id_number') }}">
                                                <div class="form-control-position">
                                                    <i class="ft-tag"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Add Mobile -->
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                            for="timesheetinput2">{{ trans('validation.attributes.mobile') }}</label>
                                        <div class="col-md-9">
                                            <div class="position-relative has-icon-left">
                                                <input type="number" required class="form-control"
                                                    placeholder="{{ trans('validation.attributes.mobile') }}" name="mobile"
                                                    value="{{ old('mobile') }}">
                                                <div class="form-control-position">
                                                    <i class="ft-tag"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Add Membership Type -->
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                            for="timesheetinput2">{{ trans('validation.attributes.sport_id') }}</label>
                                        <div class="col-md-9">
                                            <div class="position-relative has-icon-left">
                                                <select
                                                  id="membershipType"
                                                  class="form-control"
                                                  required="true"
                                                  name="sport_id"
                                                >
                                                  <option value="" selected>{{ trans('validation.attributes.sport_id') }}</option>
                                                  @foreach ($sports as $sport)
                                                      <option value="{{ $sport->id }}" {{ old('sport_id') == $sport->id ? 'selected' : '' }}>{{ $sport->title }}</option>
                                                  @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Add Gender -->
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                            for="timesheetinput2">{{ trans('validation.attributes.gender') }}</label>
                                        <div class="col-md-9">
                                            <div class="position-relative has-icon-left">
                                                <select
                                                  id="genderType"
                                                  class="form-control"
                                                  required="true"
                                                  name="gender"
                                                >
                                                  <option value="" selected> {{ trans('validation.attributes.gender') }} </option>
                                                  <option value="1" {{ old('gender') == "1" ? 'selected' : '' }}> {{ trans('admin.male') }} </option>
                                                  <option value="2" {{ old('gender') == "2" ? 'selected' : '' }}> {{ trans('admin.female') }} </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Add Nationality -->
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                            for="timesheetinput2">{{ trans('validation.attributes.nationality') }}</label>
                                        <div class="col-md-9">
                                            <div class="position-relative has-icon-left">
                                                <input type="text" required class="form-control"
                                                    placeholder="{{ trans('validation.attributes.nationality') }}" name="nationality"
                                                    value="{{ old('nationality') }}">
                                                <div class="form-control-position">
                                                    <i class="ft-tag"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Add Birth Date -->
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                            for="timesheetinput2">{{ trans('validation.attributes.birth_date') }}</label>
                                        <div class="col-md-9">
                                            <div class="position-relative has-icon-left">
                                                <input type="date" required class="form-control"
                                                    placeholder="{{ trans('validation.attributes.birth_date') }}" name="birth_date"
                                                    value="{{ old('birth_date') }}">
                                                <div class="form-control-position">
                                                    <i class="ft-tag"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Add Address -->
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                            for="timesheetinput2">{{ trans('validation.attributes.address') }}</label>
                                        <div class="col-md-9">
                                            <div class="position-relative has-icon-left">
                                                <input type="text" required class="form-control"
                                                    placeholder="{{ trans('validation.attributes.address') }}" name="address"
                                                    value="{{ old('address') }}">
                                                <div class="form-control-position">
                                                    <i class="ft-tag"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Add Image -->
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                            for="timesheetinput2">{{ trans('validation.attributes.image') }}</label>
                                        <div class="col-md-9">
                                            <div class="position-relative has-icon-left">
                                                <input type="file" required class="form-control"
                                                    name="image">
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- Save Button -->
                                <div class="form-actions right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fas fa-save"></i> {{ trans("admin.save") }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
