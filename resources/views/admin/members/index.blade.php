@extends('admin.layouts.app')
@section('content')
<div class="content-body">
    <!-- HTML5 export buttons table -->
    <!-- Column selectors table -->
    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ trans('admin.search') }}</h4>
                        <form id="search" action="{{ aurl('/members') }}" method="get" >
                            <br />
                            <input type="text"   placeholder="{{ trans('validation.attributes.name') }}"  value="{{ request()->name }}" class="form-control" style="width:15%; display:inline;" name="name" />
                            <input type="number" placeholder="{{ trans('validation.attributes.mobile') }}"  value="{{ request()->mobile }}" class="form-control" style="width:15%; display:inline;" name="mobile" />
                            <input type="number" placeholder="{{ trans('validation.attributes.id_number') }}"  value="{{ request()->id_number }}" class="form-control" style="width:15%; display:inline;" name="id_number" />
                            <select name="sport_id" class="form-control"  style="width:20%; display:inline; margin:20px;">
                                <option value="">{{ trans('validation.attributes.sport_id') }}</option>
                                @foreach ($sports as $sport)
                                    <option value="{{ $sport->id }}" {{ (request()->sport_id == $sport->id) ? "selected" : "" }}>{{ $sport->title }}</option>
                                @endforeach
                            </select>
                            <select name="gender" class="form-control"  style="width:10%; display:inline; margin:20px;">
                                <option value="">{{ trans('validation.attributes.gender') }}</option>
                                <option value="1" {{ request()->gender == "1" ? 'selected' : '' }}> {{ trans('admin.male') }} </option>
                                <option value="2" {{ request()->gender == "2" ? 'selected' : '' }}> {{ trans('admin.female') }} </option>
                            </select>
                            <select name="status" class="form-control"  style="width:10%; display:inline; margin:20px;">
                                <option value="">{{ trans('admin.status') }}</option>
                                <option value="1" {{ request()->status == "1" ? 'selected' : '' }}> {{ trans('website.active') }} </option>
                                <option value="2" {{ request()->status == "2" ? 'selected' : '' }}> {{ trans('website.unactive') }} </option>
                            </select>


                            <br />
                            <button class="btn btn-success btn-min-width mr-1 mb-1" type="submit" style="display:inline; margin:20px auto;"></i>{{ trans('admin.search') }}</button>
                        </form>
                        <hr />
                        <h4 class="card-title">{{ $title }} <span class="badge badge-danger">{{ $count }}</span></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ trans('validation.attributes.name') }}</th>
                                        <th>{{ trans('validation.attributes.id_number') }}</th>
                                        <th>{{ trans('validation.attributes.mobile') }}</th>
                                        <th>{{ trans('validation.attributes.sport_id') }}</th>
                                        <th>{{ trans('validation.attributes.birth_date') }}</th>
                                        <th>{{ trans('validation.attributes.gender') }}</th>
                                        <th>{{ trans('admin.status') }}</th>
                                        <th>{{ trans('admin.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($members as $member)
                                    <tr id="show_{{ $member->id }}" @if($member->status == "0") style="color:#FF0000" @endif>
                                        <td>{{ $loop->index+1 }}</td>
                                        <td>{{ $member->name }}</td>
                                        <td>{{ $member->id_number }}</td>
                                        <td>{{ $member->mobile }}</td>
                                        <td>{{ $member->sport->title }}</td>
                                        <td>{{ $member->birth_date }}</td>
                                        <td>{{ $member->gender == "1" ? trans('admin.male') : trans('admin.female') }}</td>
                                        <td>
                                            @if ($member->status == 1)
                                                <a href="{{ aurl('/members/active/'.$member->id) }}" class="btn btn-warning btn-min-width mr-1 mb-1"><i
                                                    class="la la-check-circle-o"></i> {{ trans('admin.unactive') }}</a>
                                            @else
                                                <a href="{{ aurl('/members/active/'.$member->id) }}" class="btn btn-success btn-min-width mr-1 mb-1"><i
                                                        class="la la-check-circle-o"></i> {{ trans('admin.active') }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ aurl('/members/'.$member->id.'/edit') }}" class="btn btn-secondary btn-min-width mr-1 mb-1"><i class="ft-edit"></i> {{ trans('admin.edit') }}</a>
                                            <input type="hidden" value="{{ csrf_token() }}" id="csrf_token" />
                                            <button class="btn btn-danger btn-min-width btn-glow mr-1 mb-1" onclick="DeleteRow({{$member->id}})"><i class="ft-delete"></i> {{ trans('admin.delete') }}</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ trans('validation.attributes.name') }}</th>
                                        <th>{{ trans('validation.attributes.id_number') }}</th>
                                        <th>{{ trans('validation.attributes.mobile') }}</th>
                                        <th>{{ trans('validation.attributes.sport_id') }}</th>
                                        <th>{{ trans('validation.attributes.birth_date') }}</th>
                                        <th>{{ trans('validation.attributes.gender') }}</th>
                                        <th>{{ trans('admin.status') }}</th>
                                        <th>{{ trans('admin.actions') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
                  <div class="pagination" style="margin:10px auto; display:table">
                      {{ $members->appends(request()->except('page'))->render() }}
                  </div>
    </section>
    <!--/ Column selectors table -->
</div>
@endsection

@section('scripts') @if (session()->get('locale') == "ar")
<script>
    function DeleteRow(id) {
                     event.preventDefault();
                     swal({
                         title: "هل انت متأكد من الحذف ؟",
                         text: "في حالة الموافقة على الحذف سيتم حذف البيانات نهائيا",
                         icon: "warning",
                         buttons: {
                             cancel: {
                                 text: "لا أريد الحذف",
                                 value: null,
                                 visible: true,
                                 className: "",
                                 closeModal: false,
                             },
                             confirm: {
                                 text: "نعم أريد الحذف",
                                 value: true,
                                 visible: true,
                                 className: "",
                                 closeModal: false
                             }
                         }
                     })
                     .then((isConfirm) => {
                         if (isConfirm) {
                          $.ajax({
                              type: 'post',
                              url: "{{aurl("/members/delete/")}}",
                              data: {
                                  '_token': $('#csrf_token').val(),
                                  'id': id,
                              },

                          });
                          $('#show_' + id).remove();
                             swal("تم الحذف!", "البيانات تم حذفها بنجاح", "success");
                         } else {
                             swal("تم الغاء الطلب", "لم يتم حذف الداتا", "error");
                         }
                     });

              }

</script>
@else
<script>
    function DeleteRow(id) {
                     event.preventDefault();
                     swal({
                         title: "Are you sure?",
                         text: "You will not be able to recover this imaginary file!",
                         icon: "warning",
                         buttons: {
                             cancel: {
                                 text: "No, cancel",
                                 value: null,
                                 visible: true,
                                 className: "",
                                 closeModal: false,
                             },
                             confirm: {
                                 text: "Yes, delete it!",
                                 value: true,
                                 visible: true,
                                 className: "",
                                 closeModal: false
                             }
                         }
                     })
                     .then((isConfirm) => {
                         if (isConfirm) {
                          $.ajax({
                              type: 'post',
                              url: "{{aurl("/members/delete/")}}",
                              data: {
                                  '_token': $('#csrf_token').val(),
                                  'id': id,
                              },

                          });
                          $('#show_' + id).remove();
                             swal("Deleted!", "Your data has been deleted.", "success");
                         } else {
                             swal("Cancelled", "Your data is safe", "error");
                         }
                     });

              }

</script>
@endif
@endsection
