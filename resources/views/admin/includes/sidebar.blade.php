<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">


            <!-- Main Page -->
            <li class="nav-item"><a href="{{ aurl('/index') }}"><i class="la la-home"></i>
                <span class="menu-title" data-i18n="nav.dash.main">{{ trans('admin.dashboard') }}</span></a>
            </li>
            <!-- Messages From Land Page -->
            <li class=" nav-item"><a href="#"><i class="icon-envelope-letter"></i>
                <span class="menu-title" data-i18n="nav.dash.main">{{ trans('admin.messages') }}</span><span class="badge badge badge-pill badge-danger float-right mr-2">{{CountMessageVisitors()}}</span></a>
            </li>


            <!-- Other Links In Menu -->
            <li class=" navigation-header">
                <span data-i18n="nav.category.support">{{ trans('admin.menu') }}</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip"
                data-placement="right" data-original-title="Support"></i>
            </li>

            <!-- Admins -->
            <li class="nav-item"><a href="#"><i class="fas fa-user-secret"></i><span class="menu-title" data-i18n="nav.templates.main">{{ trans('admin.admins') }}</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{ aurl('/admins') }}" data-i18n="nav.templates.horz.classic">{{ trans('admin.all admins') }}</a></li>
                    <li><a class="menu-item" href="{{ aurl('/admins/create') }}" data-i18n="nav.templates.horz.classic">{{ trans('admin.add admin') }}</a></li>
                </ul>
            </li>

            <!-- Members -->
            <li class="nav-item"><a href="#"><i class="fas fa-id-card"></i><span class="menu-title" data-i18n="nav.templates.main">{{ trans('admin.Members') }}</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{ aurl('/members') }}" data-i18n="nav.templates.horz.classic">{{ trans('admin.Members') }}</a></li>
                    <li><a class="menu-item" href="{{ aurl('/members/create') }}" data-i18n="nav.templates.horz.classic">{{ trans('admin.add member') }}</a></li>
                </ul>
            </li>

            <!-- Videos -->
            <li class="nav-item"><a href="#"><i class="fas fa-video"></i><span class="menu-title" data-i18n="nav.templates.main">{{ trans('admin.Videos') }}</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{ aurl('/videos') }}" data-i18n="nav.templates.horz.classic">{{ trans('admin.Videos') }}</a></li>
                    <li><a class="menu-item" href="{{ aurl('/videos/create') }}" data-i18n="nav.templates.horz.classic">{{ trans('admin.add video') }}</a></li>
                </ul>
            </li>

            <!-- Settings -->
            <li class=" navigation-header">
                <span data-i18n="nav.category.support">{{ trans('admin.settings') }}</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip"
                data-placement="right" data-original-title="Support"></i>
            </li>
            <li class=" nav-item">
                <a href="{{ aurl('/settings') }}"><i class="icon-settings"></i>
                    <span class="menu-title" data-i18n="nav.support_documentation.main">{{ trans('admin.settings') }}</span>
                </a>
            </li>

        </ul>

    </div>
</div>
