@extends('front.layouts.app')

@section('content')
    <main>
      <section class="profile-top">
        <div class="container">
          <div class="row">
            <div class="col-12 profile-head ">
              <h2 class="h4 user-name bold">تفعيل العضوية</h2>
            </div>
          </div>
        </div>
      </section>

      <section class="profile-pages">
        <div class="container">
          <div class="row ">
            <!-- main content  -->
            <div class="col-12  profile-main m-card profile-card ">
              <div class="edit-profile row">
                <div class="account col-12 col-lg-8">
                  <div class="card card-signin ">
                    <div class="card-body">
                        @include('front.includes.messages')
                      <h5 class="card-title text-center">
                        ادخل البيانات التالية
                      </h5>

                      <form class="form-signin" method="post" enctype="multipart/form-data" action="{{ url('members') }}">
                          @csrf
                        <!-- start form row -->
                        <div class="row">
                          <!-- --- full name --- -->
                          <div
                            class="form-label-group col-12 col-md-6 col-12 col-md-6"
                          >
                            <input
                              type="text"
                              id="userFullName"
                              class="form-control"
                              placeholder="الاسم رباعي"
                              required="true"
                              name="name"
                              value="{{ old('name') }}"
                              autofocus=""
                            />
                            <label for="userFullName">
                              الاسم رباعي <i>*</i></label
                            >
                          </div>

                          <!-- --- id num --- -->
                          <div
                            class="form-label-group col-12 col-md-6 col-12 col-md-6"
                          >
                            <input
                              type="number"
                              id="idNumber"
                              class="form-control"
                              placeholder="رقم الهوية"
                              required="true"
                              name="id_number"
                              value="{{ old('id_number') }}"
                              autofocus=""
                            />
                            <label for="idNumber"> رقم الهوية <i>*</i></label>
                          </div>

                           <!-- --- phone num --- -->
                           <div
                           class="form-label-group col-12 col-md-6 col-12 col-md-6"
                         >
                           <input
                             type="number"
                             id="phoneNumber"
                             class="form-control"
                             placeholder="رقم الجوال"
                             required="true"
                             name="mobile"
                             value="{{ old('mobile') }}"
                             autofocus=""
                           />
                           <label for="phoneNumber"> رقم الجوال <i>*</i></label>
                         </div>

                          <!-- --- membership type --- -->
                          <div
                            class="form-label-group col-12 col-md-6 col-12 col-md-6"
                          >
                            <select
                              id="membershipType"
                              class="form-control"
                              required="true"
                              name="sport_id"
                            >
                              <option value="" selected> فئة العضوية <i>*</i> </option>
                              @foreach ($sports as $sport)
                                  <option value="{{ $sport->id }}" {{ old('sport_id') == $sport->id ? 'selected' : '' }}>{{ $sport->title }}</option>
                              @endforeach
                            </select>
                          </div>

                          <!-- --- Gender --- -->
                          <div
                            class="form-label-group col-12 col-md-6 col-12 col-md-6"
                          >
                            <select
                              id="genderType"
                              class="form-control"
                              required="true"
                              name="gender"
                            >
                              <option value="" selected> الجنس <i>*</i> </option>
                              <option value="1" {{ old('gender') == "1" ? 'selected' : '' }}> ذكر </option>
                              <option value="2" {{ old('gender') == "2" ? 'selected' : '' }}> أنثي </option>
                            </select>
                          </div>

                          <!-- --- nationality type --- -->
                          <div
                            class="form-label-group col-12 col-md-6 col-12 col-md-6"
                          >
                          <input
                            type="text"
                            id="nationality"
                            class="form-control"
                            placeholder="الجنسية"
                            required="true"
                            name="nationality"
                            value="{{ old('nationality') }}"
                            autofocus=""
                          />
                          <label for="nationality">
                            الجنسية <i>*</i></label
                          >
                          </div>

                          <!-- --- birthDay  --- -->
                          <div
                            class="form-label-group col-12 col-md-6 col-12 col-md-6"
                          >
                            <input
                              type="date"
                              id="birthDay"
                              class="form-control"
                              placeholder="تاريخ الميلاد"
                              required="true"
                              name="birth_date"
                              value="{{ old('birth_date') }}"
                              autofocus=""
                            />
                            <label for="birthDay"> تاريخ الميلاد <i>*</i></label>
                          </div>

                          <!-- --- address  --- -->
                          <div
                            class="form-label-group col-12 col-md-6 col-12 col-md-6"
                          >
                            <input
                              type="text"
                              id="address"
                              class="form-control"
                              placeholder="العنوان"
                              required="true"
                              name="address"
                              value="{{ old('address') }}"
                              autofocus=""
                            />
                            <label for="address"> العنوان <i>*</i></label>
                          </div>



                          <!-- personal image  -->
                          <div
                            class="form-label-group col-12 col-md-6 col-12 col-md-6"
                          >
                            <input
                              type="file"
                              id="personalImg "
                              class="form-control file-upload"
                              placeholder="University Team Logo"
                              required="true"
                              name="image"
                              autofocus=""
                            />
                            <label for="personalImg" class="file-upload-label"
                              > صورة شخصية <i>*</i></label
                            >
                          </div>

                          <!-- end of row  -->
                        </div>
                        <!-- end form row -->

                        <button
                          class="btn btn-block btn-small btn-pink text-uppercase btn-round mt-3"
                          type="submit"
                        >
                          <div class="hover">
                            <span></span><span></span><span></span><span></span
                            ><span></span>
                          </div>
                          ارسال البيانات
                        </button>
                        <div class="info text-center mt-3">بالضغط على ارسال انت توافق على

                         <a href="#" class="bold">
                          الشروط والأحكام
                        </a>

                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- end  main content  -->
          </div>
        </div>
      </section>
    </main>
@endsection
