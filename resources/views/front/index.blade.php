@extends('front.layouts.app')
@section('content')
    <main>
         <section class="main-slider">
           <div
             id="carouselExampleIndicators"
             class="carousel slide home-carousel"
             data-ride="carousel"
           >
             <div class="carousel-inner" role="listbox">
               <!-- Slide One - Set the background image for this slide in the line below -->
               <div
                 class="carousel-item active"
                 style="background-image: url('{{ asset('frontend/assets/imgs/slider/slide-1.png')}}')"
               >
                 <div class="carousel-caption">
                   <div class="carousel-content wow flipInX">
                     <h1 class="h3">
                       الاتحاد السعودي للرياضات اللاسلكية والتحكم عن بعد
                     </h1>
                     <p class="lead">
                       أشعل روح التنافس
                     </p>
                     <a
                       href="#games"
                       class="btn btn-pink wow rubberBand"
                       data-wow-delay="0.9s"
                     >
                       أهم الرياضات
                     </a>
                   </div>
                 </div>
               </div>

               <!-- end slide items -->
             </div>
           </div>
         </section>

         <section id="news" class="news ">
           <div class="container">
             <div class="row">
               <div class="col-12">
                 <div class="swiper-container news-swiper-container " dir="rtl">
                   <!-- change dir to rtl in arabic  -->
                   <div class="swiper-wrapper">
                     <!-- Slides -->
                     <div class="swiper-slide m-card comp-card m-card-hor">
                       <a href="#">
                         <img
                           src="{{ asset('frontend/assets/imgs/main/news-1.jpg')}}"
                           alt="news title"
                           class="side-img"
                         />
                         <div class="card-body">
                           <h3 class="h6 reg">
                             هذا النص هو مثال لنص أن يستبدل في نفس ..
                           </h3>
                           <span class="news-time">22/02/2020</span>
                         </div>
                       </a>
                     </div>

                     <div class="swiper-slide m-card comp-card m-card-hor">
                       <a href="#">
                         <img
                           src="{{ asset('frontend/assets/imgs/main/news-2.jpg')}}"
                           alt="news title"
                           class="side-img"
                         />
                         <div class="card-body">
                           <h3 class="h6 reg">
                             هذا النص هو مثال لنص أن يستبدل في نفس ..
                           </h3>
                           <span class="news-time">22/02/2020</span>
                         </div>
                       </a>
                     </div>
                     <div class="swiper-slide m-card comp-card m-card-hor">
                       <a href="#">
                         <img
                           src="{{ asset('frontend/assets/imgs/main/news-3.png')}}"
                           alt="news title"
                           class="side-img"
                         />
                         <div class="card-body">
                           <h3 class="h6 reg">
                             هذا النص هو مثال لنص أن يستبدل في نفس ..
                           </h3>
                           <span class="news-time">22/02/2020</span>
                         </div>
                       </a>
                     </div>
                     <div class="swiper-slide m-card comp-card m-card-hor">
                       <a href="#">
                         <img
                           src="{{ asset('frontend/assets/imgs/main/news-4.jpg')}}"
                           alt="news title"
                           class="side-img"
                         />
                         <div class="card-body">
                           <h3 class="h6 reg">
                             هذا النص هو مثال لنص أن يستبدل في نفس ..
                           </h3>
                           <span class="news-time">22/02/2020</span>
                         </div>
                       </a>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="col-12 news-cta">
                 <a href="#"> المزيد من الأخبار </a>
               </div>
             </div>
           </div>
         </section>

         <section class="games" id="games">
           <div class="container">
             <div class="row">
               <div class="col-12 ">
                 <div class="main-title ">
                   <h2 class="title h3 text-center wow fadeInDown">
                     الرياضات المعتمدة
                   </h2>
                 </div>
               </div>
               <div class="col-12">
                 <div class="swiper-container games-swiper-container " dir="rtl">
                   <!-- change dir to rtl in arabic  -->
                   <div class="swiper-wrapper">
                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card game-card">
                       <a
                         href="#"
                         class="game-link"
                         style="background-image: url('{{ asset('frontend/assets/imgs/main/games-1.png')}}');"
                       >
                         <span class="btn-arrow">
                           <i class="fas fa-arrow-right"></i>
                         </span>
                         <div class="game-icon">
                           <img
                             src="{{ asset('frontend/assets/imgs/icons/game-icon-1.png')}}"
                             alt="Game icon"
                           />
                         </div>
                         <h4 class="game-title h5 reg">
                           الطائرات اللاسلكية
                         </h4>
                       </a>
                     </div>

                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card game-card">
                       <a
                         href="#"
                         class="game-link"
                         style="background-image: url('{{ asset('frontend/assets/imgs/main/games-2.jpg')}}');"
                       >
                         <span class="btn-arrow">
                           <i class="fas fa-arrow-right"></i>
                         </span>
                         <div class="game-icon">
                           <img
                             src="{{ asset('frontend/assets/imgs/icons/game-icon-2.png')}}"
                             alt="Game icon"
                           />
                         </div>
                         <h4 class="game-title h5 reg">
                           السيارات اللاسلكية
                         </h4>
                       </a>
                     </div>

                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card game-card">
                       <a
                         href="#"
                         class="game-link"
                         style="background-image: url('{{ asset('frontend/assets/imgs/main/games-3.png')}}');"
                       >
                         <span class="btn-arrow">
                           <i class="fas fa-arrow-right"></i>
                         </span>
                         <div class="game-icon">
                           <img
                             src="{{ asset('frontend/assets/imgs/icons/game-icon-3.png')}}"
                             alt="Game icon"
                           />
                         </div>
                         <h4 class="game-title h5 reg">
                           القوارب اللاسلكية
                         </h4>
                       </a>
                     </div>

                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card game-card">
                       <a
                         href="#"
                         class="game-link"
                         style="background-image: url('{{ asset('frontend/assets/imgs/main/gams-4.jpg')}}');"
                       >
                         <span class="btn-arrow">
                           <i class="fas fa-arrow-right"></i>
                         </span>
                         <div class="game-icon">
                           <img
                             src="{{ asset('frontend/assets/imgs/icons/game-icon-4.png')}}"
                             alt="Game icon"
                           />
                         </div>
                         <h4 class="game-title h5 reg">
                           الروبوتات
                         </h4>
                       </a>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </section>

         <section class="competition-sec" id="competition-sec">
           <div class="container-fluid-comp">
             <div class="row">
               <div class="col-12 ">
                 <div class="main-title">
                   <h2 class="title h3 text-center wow fadeInDown">
                     المسابقات
                   </h2>
                 </div>
               </div>
               <div class="col-12">
                 <div class="swiper-container comp-swiper-container " dir="rtl">
                   <!-- change dir to rtl in arabic  -->
                   <div class="swiper-wrapper">
                     <!-- for current active events add+ "comp-card-active" class  -->

                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card ">
                       <div class="comp-card-head">
                         <img src="{{ asset('frontend/assets/imgs/main/fll.png')}}" alt="" />
                       </div>
                       <div class="comp-card-body">
                         <h4 class="game-title h5 reg">
                           الطائرات اللاسلكية
                         </h4>
                         <span class="comp-time"> 22/02/2020 </span>

                         <div class="comp-cta">
                           <a href="#"> المزيد </a>
                         </div>
                       </div>
                     </div>

                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card ">
                       <div class="comp-card-head">
                         <img src="{{ asset('frontend/assets/imgs/main/fll.png')}}" alt="" />
                       </div>
                       <div class="comp-card-body">
                         <h4 class="game-title h5 reg">
                           الطائرات اللاسلكية
                         </h4>
                         <span class="comp-time"> 22/02/2020 </span>

                         <div class="comp-cta">
                           <a href="#"> المزيد </a>
                         </div>
                       </div>
                     </div>

                     <!-- Slide || active slide use "comp-card-active"  -->
                     <div class="swiper-slide m-card comp-card comp-card-active ">
                       <div class="comp-card-head">
                         <img src="{{ asset('frontend/assets/imgs/main/fll.png')}}" alt="" />
                       </div>
                       <div class="comp-card-body">
                         <h4 class=" h5 reg">
                           الطائرات اللاسلكية
                         </h4>
                         <span class="comp-time"> 22/02/2020 </span>

                         <div class="comp-cta">
                           <a href="#"> المزيد </a>
                         </div>
                       </div>
                     </div>

                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card ">
                       <div class="comp-card-head">
                         <img src="{{ asset('frontend/assets/imgs/main/fll.png')}}" alt="" />
                       </div>
                       <div class="comp-card-body">
                         <h4 class="game-title h5 reg">
                           الطائرات اللاسلكية
                         </h4>
                         <span class="comp-time"> 22/02/2020 </span>

                         <div class="comp-cta">
                           <a href="#"> المزيد </a>
                         </div>
                       </div>
                     </div>

                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card ">
                       <div class="comp-card-head">
                         <img src="{{ asset('frontend/assets/imgs/main/fll.png')}}" alt="" />
                       </div>
                       <div class="comp-card-body">
                         <h4 class="game-title h5 reg">
                           الطائرات اللاسلكية
                         </h4>
                         <span class="comp-time"> 22/02/2020 </span>

                         <div class="comp-cta">
                           <a href="#"> المزيد </a>
                         </div>
                       </div>
                     </div>

                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card ">
                       <div class="comp-card-head">
                         <img src="{{ asset('frontend/assets/imgs/main/fll.png')}}" alt="" />
                       </div>
                       <div class="comp-card-body">
                         <h4 class="game-title h5 reg">
                           الطائرات اللاسلكية
                         </h4>
                         <span class="comp-time"> 22/02/2020 </span>

                         <div class="comp-cta">
                           <a href="#"> المزيد </a>
                         </div>
                       </div>
                     </div>

                     <!-- Slide  -->
                     <div class="swiper-slide m-card comp-card ">
                       <div class="comp-card-head">
                         <img src="{{ asset('frontend/assets/imgs/main/fll.png')}}" alt="" />
                       </div>
                       <div class="comp-card-body">
                         <h4 class="game-title h5 reg">
                           الطائرات اللاسلكية
                         </h4>
                         <span class="comp-time"> 22/02/2020 </span>

                         <div class="comp-cta">
                           <a href="#"> المزيد </a>
                         </div>
                       </div>
                     </div>

                     <!-- slides end  -->
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </section>

         <section class="videos">
           <div class="container ">
             <div class="row">
               <div class="col-12 col-md-4 videos-title">
                 <div class="main-title">
                   <h2 class="title h3 wow fadeInDown">
                     قناة الاتحاد
                   </h2>
                   <a href="#" class="cta"> المزيد </a>
                 </div>
               </div>
             </div>
           </div>
           <div class="container vds-container">
             <div class="row">
               <!-- ///////////////////// -->
               <div
                 dir="rtl"
                 class="swiper-container swiper-container-videos gallery-top col-12 col-lg-9"
               >
                 <div class="swiper-wrapper">
                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=PR-zZcumWlA"
                   />

                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=E47X2kTv84E&t=3s"
                   />

                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=ZiEvRAitPDs"
                   />

                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=p7UWY6CpL5c"
                   />

                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=ivny747cYpM"
                   />
                 </div>
                 <!-- Add Arrows -->
                 <div class="swiper-button-next swiper-button-white"></div>
                 <div class="swiper-button-prev swiper-button-white"></div>
               </div>
               <!-- -----  -->
               <div class="swiper-container gallery-thumbs col-12 col-lg-3">
                 <div class="swiper-wrapper">
                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=PR-zZcumWlA"
                   />

                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=E47X2kTv84E&t=3s"
                   />

                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=ZiEvRAitPDs"
                   />

                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=p7UWY6CpL5c"
                   />

                   <img
                     class="swiper-slide"
                     src="{{ asset('frontend/assets/imgs/main/video-placeholder.png')}}"
                     data-yt="https://www.youtube.com/watch?v=ivny747cYpM"
                   />
                 </div>

                 <div class="swiper-scrollbar"></div>
               </div>

               <!-- ////////////  -->
             </div>
           </div>
           <div class="video-pop d-none ">
             <i class="fas fa-times close-icons"></i>
             <iframe
               src="https://www.youtube.com/embed/z9Ul9ccDOqE"
               frameborder="0"
               allowfullscreen
             >
             </iframe>
           </div>
         </section>

         <section class="sponsers">
           <div class="container ">
             <div class="row">
               <div class="col-12">
                 <div class="main-title text-center">
                   <h2 class="title text-center h3 wow fadeInDown">
                     الرعاة
                   </h2>
                 </div>
               </div>
             </div>
           </div>

           <div class="container">
             <div class="row">
               <div class="col-12">
                 <div class="swiper-container sponser-swiper-container " dir="rtl">
                   <!-- change dir to rtl in arabic  -->
                   <div class="swiper-wrapper">
                     <!-- Slides -->
                     <img
                       src="{{ asset('frontend/assets/imgs/main/besteam-logo.png')}}"
                       class="swiper-slide"
                     />
                     <img
                       src="{{ asset('frontend/assets/imgs/main/MCIT_logo.png')}}"
                       class="swiper-slide"
                     />
                     <img src="{{ asset('frontend/assets/imgs/main/stc.png')}}" class="swiper-slide" />
                     <img src="{{ asset('frontend/assets/imgs/main/ndu.png')}}" class="swiper-slide" />
                     <img
                       src="{{ asset('frontend/assets/imgs/main/wro-logo.png')}}"
                       class="swiper-slide"
                     />
                   </div>
                   <div class="swiper-pagination"></div>
                 </div>
               </div>
             </div>
           </div>
         </section>
       </main>
@endsection
