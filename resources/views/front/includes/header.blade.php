<!-- header  -->

    <header class="header">
      <!-- Navbar-->
      <nav class="navbar navbar-expand-lg fixed-top m-card navbar-light ">
        <div class="container-fluid">
          <div class="d-flex justify-content-center">
            <a href="{{ url('/') }}" class="navbar-brand"
              ><img
                src="{{ asset('frontend/assets/imgs/main/tahakom-logo.png')}}"
                alt=" Tahakom logo"
            /></a>
          </div>

          <button
            type="button"
            data-toggle="collapse"
            data-target="#navbarCollapse"
            aria-controls="navbarCollapse"
            aria-expanded="false"
            aria-label="Toggle navigation"
            class="navbar-toggler navbar-toggler-right"
          >
            <svg viewBox="0 0 64 48">
              <path d="M19,15 L45,15 C70,15 58,-2 49.0177126,7 L19,37"></path>
              <path d="M19,24 L45,24 C61.2371586,24 57,49 41,33 L32,24"></path>
              <path d="M45,33 L19,33 C-8,33 6,-2 22,14 L45,37"></path>
            </svg>
          </button>

          <div class="nav-layers d-flex flex-column">
            <div class="nav-top ">
              <div class="social-icons">
                <a href="#" class="social-icons-link">
                  <i class="fab fa-facebook-f"></i>
                </a>
                <a href="#" class="social-icons-link">
                  <i class="fab fa-twitter"></i>
                </a>
                <a href="#" class="social-icons-link">
                  <i class="fab fa-instagram"></i>
                </a>
                <!-- <a href="#" class="social-icons-link"> <i class="fas fa-search"></i> </a> -->

                <!-- ////////////////  -->
              </div>
              <ul class="navbar-nav icons-nav nav-top-links ">
                <li class="nav-item nav-icon dropdown dropdown-lang">
                  <a
                    id="homeDropdownMenuLink"
                    href="#"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    class="nav-link dropdown-toggle"
                  >
                    <span class="default-lang">
                      <img src="{{ asset('frontend/assets/imgs/icons/saudi-flag.svg')}}" alt="" />
                      <span class="d-none d-lg-inline">اللغة العربية</span>
                    </span>
                  </a>
                  <div
                    aria-labelledby="homeDropdownMenuLink"
                    class="dropdown-menu"
                  >
                    <a href="#" class="dropdown-item">
                      <span>
                        <img src="{{ asset('frontend/assets/imgs/icons/saudi-flag.svg')}}" alt="" />
                        <span>اللغة العربية</span>
                      </span>
                    </a>
                    <a href="#" class="dropdown-item">
                      <span>
                        <img src="{{ asset('frontend/assets/imgs/icons/uk-flag.svg')}}" alt="" />
                        <span>English</span>
                      </span>
                    </a>
                  </div>
                </li>
                <li class="nav-item d-none d-lg-inline">
                  <a href="mailto:Info@tahakoom.gov.sa " class="nav-link">
                    <i class="fas fa-envelope"></i>
                    <span>Info@tahakoom.gov.sa </span>
                  </a>
                </li>
              </ul>
            </div>
            <h1 class="nav-middle f-14 ">
              الاتحاد السعودي للرياضات اللاسلكية والتحكم عن بعد | Saudi Wireless
              & Remote Control Sports Federation & Robot
            </h1>

            <!-- Navbar Collapse -->
            <div
              id="navbarCollapse"
              class="collapse navbar-collapse nav-bottom wow slideDown "
            >
              <ul class="navbar-nav middle-nav">
                <!-- /// use "active" class for current page  ////  -->
                <li class="nav-item">
                  <a href="{{ url('/') }}" class="home-nav-link nav-link">
                    <i class=" fas fa-home"></i>
                  </a>
                </li>

                <li class="nav-item dropdown">
                  <a
                    id="homeDropdownMenuLink"
                    href asset="frontend"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    class="nav-link dropdown-toggle"
                  >
                    الرياضات المعتمدة
                  </a>
                  <div
                    aria-labelledby="homeDropdownMenuLink"
                    class="dropdown-menu"
                  >
                    <a href="#" class="dropdown-item"> الطائرات اللاسلكية </a>
                    <a href="#" class="dropdown-item"> السيارات اللاسلكية </a>
                    <a href="#" class="dropdown-item"> القوارب اللاسلكية </a>
                    <a href="#" class="dropdown-item"> الروبوتات </a>
                  </div>
                </li>

                <li class="nav-item">
                  <a href="#" class="nav-link "> المسابقات </a>
                </li>

                <li class="nav-item">
                  <a href="#" class="nav-link"> الأخبار </a>
                </li>

                <li class="nav-item">
                  <a href="{{ url('members/create') }}" class="nav-link active"> تسجيل عضوية </a>
                </li>

                <li class="nav-item">
                  <a href="#" class="nav-link"> اللوائح والقوانين </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <!-- /Navbar -->
    </header>
    <!-- End header  -->
