<footer class="footer ">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 col-lg-3 ">
        <h4 class="footer-title h5">
          تواصل معنا
        </h4>
        <ul class="footer-content svg-icons">
          <li>
            <img src="{{ asset('frontend/assets/imgs/icons/whatsapp.svg')}}" alt="" />
            <span>
              <a href="tel:0555069800"> 0555069800 </a>
            </span>
          </li>
          <li>
            <img src="{{ asset('frontend/assets/imgs/icons/telephone.svg')}}" alt="" />
            <span> <a href="tel:920020241"> 920020241 </a> </span>
          </li>
          <li>
            <img src="{{ asset('frontend/assets/imgs/icons/mail.svg')}}" alt="" />
            <span>
              <a href="mailto:Info@tahakoom.gov.sa ">
                info@tahakoom.gov.sa
              </a>
            </span>
          </li>
          <li>
            <img src="{{ asset('frontend/assets/imgs/icons/internet.svg')}}" alt="" />
            <span>
              <a href="https://tahakoom.gov.sa">
                www.tahakoom.gov.sa
              </a></span
            >
          </li>
        </ul>
      </div>
      <div class="col-12 col-md-6 col-lg-3 ">
        <h4 class="footer-title h5">
          القائمة
        </h4>
        <ul class="footer-content menu">
          <li>
            <i class="fas fa-dot-circle fa-xs"> </i>
            <span> <a href="#"> Home </a> </span>
          </li>
          <li>
            <i class="fas fa-dot-circle fa-xs"> </i>
            <span> <a href="#"> Shop </a> </span>
          </li>
          <li>
            <i class="fas fa-dot-circle fa-xs"> </i>
            <span> <a href="#"> Contact </a> </span>
          </li>
          <li>
            <i class="fas fa-dot-circle fa-xs"> </i>
            <span> <a href="#"> login </a> </span>
          </li>
        </ul>
      </div>
      <div class="col-12 col-md-6 col-lg-3 ">
        <h4 class="footer-title h5">
          Competition
        </h4>
        <ul class="footer-content svg-icons">
          <li>
            <img src="{{ asset('frontend/assets/imgs/icons/award.svg')}}" alt="" />
            <span> <a href="#"> Open Category </a></span>
          </li>
          <li>
            <img src="{{ asset('frontend/assets/imgs/icons/award.svg')}}" alt="" />
            <span> <a href="#"> Football Category </a> </span>
          </li>
          <li>
            <img src="{{ asset('frontend/assets/imgs/icons/award.svg')}}" alt="" />
            <span> <a href="#"> Advanced Robotics Challenge </a> </span>
          </li>
          <li>
            <img src="{{ asset('frontend/assets/imgs/icons/award.svg')}}" alt="" />
            <span> <a href="#"> Regular Category </a> </span>
          </li>
        </ul>
      </div>
      <div class="col-12 col-md-6 col-lg-3 ">
        <h4 class="footer-title h5">
          Training & Support
        </h4>
        <ul class="footer-content svg-icons">
          <li>
            <img src="{{ asset('frontend/assets/imgs/icons/download.svg')}}" alt="" />
            <span> <a href="#"> Download Training Support Files </a></span>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <hr class="footer-split" />
  <div class="container">
    <div class="row">
      <div class="col-12 footer-sub">
        <div class="social-icons">
          <a href="#" class="social-icons-link">
            <i class="fab fa-facebook-f"></i>
          </a>
          <a href="#" class="social-icons-link">
            <i class="fab fa-twitter"></i>
          </a>
          <a href="#" class="social-icons-link">
            <i class="fab fa-instagram"></i>
          </a>
        </div>

        <div class="copy">
          <img
            src="{{ asset('frontend/assets/imgs/main/tahakom-white.png')}}"
            alt="tahakom logo"
          />
          <span>
            تحكم © 2020
          </span>
        </div>

        <div class="mobile-store">
          <a href="#"> <img src="{{ asset('frontend/assets/imgs/icons/app-store.png')}}" alt="download mobile app"> </a>
          <a href="#"> <img src="{{ asset('frontend/assets/imgs/icons/play-store.png')}}" alt="download mobile app"> </a>
        </div>
      </div>
    </div>
  </div>
</footer>
