<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <link rel="shortcut icon" href="{{ asset('frontend/assets/imgs/main/fav-icon.png')}}" />
    <title>الاتحاد السعودي للرياضات اللاسلكية والتحكم عن بعد</title>

    <!-- fontawesome icons  -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/fonts/fontawesome/css/all.min.css')}}" />

    <!-- bootstrap  -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/bootstrap.min.css')}}" />

    <!-- swiper slider  -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css" />

    <!-- main style  -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/style.css')}}" />
    <!-- English style  -->
    <!-- <link rel="stylesheet" href="./assets/css/ltr.css" /> -->

    <!-- Arabic style  -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/rtl.css')}}" />
  </head>
  <body>

  @include('front.includes.header')

  
      @yield('content')
  @include('front.includes.footer')

  <!-- external scripts  -->
    <script src="{{ asset('frontend/assets/js/wow.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/jquery.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/popper.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/main.js')}}"></script>

    <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
    <!-- news slider  -->
    <script>
      var swiper = new Swiper(".news-swiper-container", {
        // slidesPerView: 4,
        // spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        },
        autoplay: {
          delay: 2500,
          disableOnInteraction: false
        },
        breakpoints: {
          576: {
            slidesPerView: 1,
            spaceBetween: 20
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 20
          },
          992: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        }
      });
    </script>
    <!-- games slider  -->
    <script>
      var gamesSwiper = new Swiper(".games-swiper-container", {
        // slidesPerView: 4,
        // spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        },
        autoplay: {
          delay: 2500,
          disableOnInteraction: false
        },
        breakpoints: {
          576: {
            slidesPerView: 1,
            spaceBetween: 20
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 20
          },
          992: {
            slidesPerView: 4,
            spaceBetween: 20
          }
        }
      });
    </script>
    <!-- competions slider  -->
    <script>
      var gamesSwiper = new Swiper(".comp-swiper-container", {
        // slidesPerView: 4,
        // spaceBetween: 30,
        // slidesPerView: "auto",
        centeredSlidesBounds: true,
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        },
        autoplay: {
          delay: 2500
        },
        breakpoints: {
          576: {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: {
              delay: 500
            }
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 20,
            autoplay: {
              delay: 2500
            }
          },
          992: {
            slidesPerView: "auto",
            spaceBetween: 20,
            autoplay: false
          }
        }
      });
    </script>
    <!-- videos slider  -->
    <script>
      var galleryThumbs = new Swiper(".gallery-thumbs", {
        // freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        direction: "vertical",
        scrollbar: {
          el: ".swiper-scrollbar",
          hide: true
        },
        breakpoints: {
          0: {
            slidesPerView: 3,
            spaceBetween: 5,
            direction: "horizontal"
          },
          576: {
            slidesPerView: 2,
            spaceBetween: 20,
            direction: "horizontal"
          },
          768: {
            slidesPerView: 4,
            spaceBetween: 20,
            direction: "horizontal"
          },
          992: {
            spaceBetween: 0,
            slidesPerView: 3,
            direction: "vertical"
          }
        }
      });
      var galleryTop = new Swiper(".gallery-top", {
        spaceBetween: 10,
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev"
        },
        thumbs: {
          swiper: galleryThumbs
        }
      });
    </script>
    <!-- video player    -->
    <script>
      $(function() {
        // console.log($(".videos .swiper-slide").attr("data-yt"));

        $(".videos .swiper-slide").each((i, el) => {
          // console.log( el.attributes['data-yt'].value  )
          // console.log(  el.attributes['data-yt'].value );

          let vidImgId = el.attributes["data-yt"].value
            .split("?v=")[1]
            .split("&")[0];

          let vidImg;
          if (vidImgId) {
            vidImg = `https://img.youtube.com/vi/${vidImgId}/0.jpg`;
          } else {
            vidImg = "./assets/imgs/main/video-placeholder.png";
          }
          // set src img
          $(el).attr("src", vidImg);
          // console.log($(el).attr("src") );
        });

        // open the video
        $(" .gallery-top .swiper-slide").click(() => {
          // console.log( $(".gallery-top .swiper-slide-active").attr("data-yt"));
          // console.log("ddddddddd");
          let activeVid = $(".gallery-top .swiper-slide-active")
            .attr("data-yt")
            .replace("watch?v=", "embed/")
            .split("&")[0];
          // https://www.youtube.com/embed/z9Ul9ccDOqE
          // https://www.youtube.com/watch?v=PR-zZcumWlA
          $(".video-pop iframe").attr("src", activeVid);
          $(".video-pop").removeClass("d-none");
        });
        $(".video-pop .close-icons").click(() => {
          $(".video-pop").addClass("d-none");
        });
      });
    </script>
    <!-- news slider  -->
    <script>
      var swiper = new Swiper(".sponser-swiper-container", {
        // slidesPerView: 4,
        // spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        },
        autoplay: {
          delay: 2500,
          disableOnInteraction: false
        },
        breakpoints: {
          576: {
            slidesPerView: 2,
            spaceBetween: 10
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 20
          },
          992: {
            slidesPerView: 4,
            spaceBetween: 30
          }
        }
      });
    </script>
  </body>
</html>
